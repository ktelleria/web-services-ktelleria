package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import java.util.*;

/**
 * Usamos anotaciones de validación de JavaBeans; introducidas en JavaEE6
 * @author fmedina
 *
 */
public class ReservacionDetalle 
{
	private Integer id;
    
    private String desde;
   
    private String hasta;

    private Cuarto cuarto;

    private Huesped huesped;


    

    public ReservacionDetalle(final Integer id, final String desde, final String hasta, final Cuarto cuarto, final Huesped huesped)
    {
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }

    public Integer getId() {
        return id;
    }

    public String getDesde() {
        return desde;
    }

    public String getHasta() {
        return hasta;
    }

    public Cuarto getCuarto() {
        return cuarto;
    }

    public Huesped getHuesped() {
        return huesped;
    }
}