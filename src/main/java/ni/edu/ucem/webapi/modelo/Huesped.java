package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Usamos anotaciones de validación de JavaBeans; introducidas en JavaEE6
 * @author fmedina
 *
 */
public class Huesped 
{
	private Integer id;
    
    @NotNull
    @NotEmpty(message = "El nombre del huesped es requerido")
    private String nombre;
    
    @NotNull
    @NotEmpty(message = "El correo es requerido.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "El correo es incorrecto")
    private String email;
    
    @NotNull(message = "El telefono es requerido.")   
    private String telefono;

    public Huesped()
    {
    }
    
    public Huesped(final String nombre, final String email, final String telefono) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(final String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(final String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(final String telefono) {
        this.telefono = telefono;
    }

}