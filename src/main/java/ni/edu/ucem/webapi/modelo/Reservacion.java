package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.math.BigInteger;
import org.hibernate.validator.constraints.NotEmpty;
import java.util.*;

/**
 * Usamos anotaciones de validación de JavaBeans; introducidas en JavaEE6
 * @author fmedina
 *
 */
public class Reservacion 
{
	private Integer id;
    
    @NotNull
    private Date desde;
    
    @NotNull
    private Date hasta;
    
    @NotNull(message = "El numero de cuarto es requerido.") 
    @Digits(integer=6, fraction=2, message = "El numero de cuarto debe ser un numero entero.")  
    private Integer cuarto;

    @NotNull(message = "El numero de huesped es requerido.")   
    private Integer huesped;

    public Reservacion()
    {
    }
    
    public Reservacion(final Date desde, final Date hasta, final Integer cuarto, final Integer huesped) {
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDesde() {
        return desde;
    }
    public void setDesde(final Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }
    public void setHasta(final Date hasta) {
        this.hasta = hasta;
    }

    public Integer getCuarto() {
        return cuarto;
    }
    public void setCuarto(final Integer cuarto) {
        this.cuarto = cuarto;
    }

    public Integer getHuesped() {
        return huesped;
    }
    public void setHuesped(final Integer huesped) {
        this.huesped = huesped;
    }

}