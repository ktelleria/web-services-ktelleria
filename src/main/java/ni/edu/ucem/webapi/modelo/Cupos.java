package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import java.util.*;
/**
 * Usamos anotaciones de validación de JavaBeans; introducidas en JavaEE6
 * @author fmedina
 *
 */
public class Cupos 
{
    @NotNull
    private String fechaIngreso;
    
    @NotNull
    private String fechaSalida;
    
    private Pagina<Cuarto> cuartos;

    public Cupos()
    {
    }
    
    public Cupos(final String fechaIngreso, final String fechaSalida, final Pagina<Cuarto> cuartos) 
    {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;     
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }
    public void setFechaIngreso(final String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }
    public void setFechaSalida(final String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Pagina<Cuarto> getCuartos() {
        return cuartos;
    }
    public void setCuartos(final Pagina<Cuarto> cuartos) {
        this.cuartos = cuartos;
    }

}