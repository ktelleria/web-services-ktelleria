package ni.edu.ucem.webapi.web.reservacion;

import javax.validation.Valid;
import java.util.*;
import java.text.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionDetalle;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.serviceImpl.ReservacionServiceImpl;


@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionResource 
{
	private final ReservacionServiceImpl reservacionService;

    String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String patternShortDate = "yyyy-MM-dd";

    SimpleDateFormat iso8601Format = new SimpleDateFormat(pattern);
    SimpleDateFormat simpleFormatDate = new SimpleDateFormat(patternShortDate);

	@Autowired
    public ReservacionResource(final ReservacionServiceImpl reservacionService)
    {
        this.reservacionService = reservacionService;
    }

    //POST
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReservacion(@Valid @RequestBody final Reservacion reservacion, BindingResult result) 
    throws Exception, NumberFormatException 
    {
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }

        Date _fechaIngreso = new Date();
        Date _fechaSalida = new Date();

        try 
        {
            _fechaIngreso = iso8601Format.parse(iso8601Format.format(reservacion.getDesde()));
            _fechaSalida = iso8601Format.parse(iso8601Format.format(reservacion.getHasta()));
        }
        catch (ParseException e) {
            throw new ParseException("Error en el formato de fechas.",e.getErrorOffset());
        }

        if(_fechaIngreso.before(new Date()))
        {
            throw new IllegalArgumentException("La fecha de ingreso debe ser mayor a la actual");
        }

        if(_fechaSalida.before(_fechaIngreso) && !(_fechaSalida.equals(_fechaIngreso)))
        {
            throw new IllegalArgumentException("La fecha de salida debe ser mayor o igual a la de ingreso");
        }
            
        try 
        {
            Huesped huesped = reservacionService.obtenerHuesped(reservacion.getHuesped());
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("El huesped no existe");
        }


        this.reservacionService.agregarReservacion(reservacion);
        return new ApiResponse(Status.OK, reservacion);
    }

    //GET
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
       final ReservacionDetalle reservacion = this.reservacionService.obtenerReservacion(id);
       return new ApiResponse(Status.OK, reservacion);
    }

}