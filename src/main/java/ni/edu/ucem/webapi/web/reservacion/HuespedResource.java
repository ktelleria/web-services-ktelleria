package ni.edu.ucem.webapi.web.reservacion;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.serviceImpl.ReservacionServiceImpl;


@RestController
@RequestMapping("/v1/huespedes")
public class HuespedResource 
{
	private final ReservacionServiceImpl reservacionService;

	@Autowired
    public HuespedResource(final ReservacionServiceImpl reservacionService)
    {
        this.reservacionService = reservacionService;
    }

    //POST
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarHuesped(@Valid @RequestBody final Huesped huesped, BindingResult result) 
    {
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        this.reservacionService.agregarHuesped(huesped);
        return new ApiResponse(Status.OK, huesped);
    }

    //GET
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
     public ApiResponse obtener(@PathVariable("id") final int id)
    {
        final Huesped huesped = this.reservacionService.obtenerHuesped(id);
        return new ApiResponse(Status.OK, huesped);
    }

}