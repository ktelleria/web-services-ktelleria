package ni.edu.ucem.webapi.dao;

import ni.edu.ucem.webapi.modelo.Huesped;

public interface HuespedDAO 
{
   	public void agregar(final Huesped pHuedped);

   	public Huesped obtenerPorId(final int pId);
}
