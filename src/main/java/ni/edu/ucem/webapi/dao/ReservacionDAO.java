package ni.edu.ucem.webapi.dao;

import java.util.ArrayList;
import java.util.List;
import ni.edu.ucem.webapi.modelo.Reservacion;

public interface ReservacionDAO 
{
   	public void agregar(final Reservacion reservacion);

   	public Reservacion obtenerPorId(final int pId);

   	public List<Reservacion> obtenerTodasPorFechas(final String fechaingreso, final String fechasalida); 
}
