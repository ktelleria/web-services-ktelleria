package ni.edu.ucem.webapi.serviceImpl;

import java.util.*;
import java.text.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionDetalle;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Cupos;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.service.ReservacionService;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;

@Service
public class ReservacionServiceImpl implements ReservacionService 
{
	private final HuespedDAO huespedDAO;
    private final ReservacionDAO reservacionDAO;
    private final CuartoDAO cuartoDAO;
    private final InventarioServiceImpl inventarioService;

    String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String patternShortDate = "yyyy-MM-dd";

    SimpleDateFormat iso8601Format = new SimpleDateFormat(pattern);
    SimpleDateFormat simpleFormatDate = new SimpleDateFormat(patternShortDate);

    public ReservacionServiceImpl(final HuespedDAO huespedDAO,final ReservacionDAO reservacionDAO, 
        CuartoDAO cuartoDAO, InventarioServiceImpl inventarioService)
    {
        this.huespedDAO = huespedDAO;
        this.reservacionDAO = reservacionDAO;
        this.cuartoDAO = cuartoDAO;
        this.inventarioService = inventarioService;
    }

    @Transactional
    @Override
    public void agregarHuesped(final Huesped pHuesped) 
    {
        this.huespedDAO.agregar(pHuesped);
    }

    @Override
    public Huesped obtenerHuesped(final int pId) 
    {
        return this.huespedDAO.obtenerPorId(pId);
    }

    @Transactional
    @Override
    public void agregarReservacion(final Reservacion pReservacion)
    {
        this.reservacionDAO.agregar(pReservacion);
    }

    @Transactional
    public ReservacionDetalle obtenerReservacion(final int pId)
    {
        Reservacion getReservacion = this.reservacionDAO.obtenerPorId(pId);

        ReservacionDetalle reservacionDetalle = new ReservacionDetalle(
                getReservacion.getId(),
                iso8601Format.format(getReservacion.getDesde()),
                iso8601Format.format(getReservacion.getHasta()),
                this.cuartoDAO.obtenerPorId(getReservacion.getCuarto()),
                this.huespedDAO.obtenerPorId(getReservacion.getHuesped())
            );

        return reservacionDetalle;
    }

    public Cupos obtenerCupos(final Date fechaIngreso, final Date fechaSalida, final Integer categoria,
        final Integer offset, final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
                .paginacion(offset, limit)
                .build();

        Pagina<Cuarto> pagina;

        if(categoria > 0)
        {
            pagina = this.inventarioService.obtenerTodosCuartoEnCategoria(categoria, paginacion);
        }
        else
        {
            pagina = this.inventarioService.obtenerTodosCuarto(paginacion);
        }    

        List<Reservacion> todasReservaciones = this.reservacionDAO.obtenerTodasPorFechas(
            simpleFormatDate.format(fechaIngreso),
            simpleFormatDate.format(fechaSalida)
            );

        List<Cuarto> cuartosDisponibles = new ArrayList<Cuarto>();

        for(Cuarto cuartoIterador : pagina.getData()) 
        {
            boolean cuartoReservado = false;

            for(Reservacion cuartoRIterador : todasReservaciones) 
            {
               if(cuartoIterador.getId() == cuartoRIterador.getCuarto())
               {
                    cuartoReservado = true;
               } 
            }

            if(!cuartoReservado)
            {
                cuartosDisponibles.add(cuartoIterador);
            }
        }  

        pagina.setData(cuartosDisponibles);

        return new Cupos(iso8601Format.format(fechaIngreso),iso8601Format.format(fechaSalida),pagina);
    }
}