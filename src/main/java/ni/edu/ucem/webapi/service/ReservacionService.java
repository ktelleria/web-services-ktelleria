package ni.edu.ucem.webapi.service;

import java.util.*;
import java.text.*;

import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionDetalle;
import ni.edu.ucem.webapi.modelo.Cupos;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;

public interface ReservacionService 
{
	public void agregarHuesped(final Huesped pHuesped);

	public Huesped obtenerHuesped(final int pId);

	public void agregarReservacion(final Reservacion pReservacion);

	public ReservacionDetalle obtenerReservacion(final int pId);

	public Cupos obtenerCupos(final Date fechaIngreso, final Date fechaSalida, final Integer categoria,
		final Integer offset, final Integer limit);

}