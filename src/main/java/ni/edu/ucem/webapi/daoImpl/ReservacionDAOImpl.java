package ni.edu.ucem.webapi.daoImpl;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Reservacion;

@Repository
public class ReservacionDAOImpl implements ReservacionDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void agregar(final Reservacion pReservacion)
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO reservacion")
                .append(" ")
                .append("(desde, hasta, cuarto, huesped)")
                .append(" ")
                .append("VALUES (?, ?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        this.jdbcTemplate.update(sql,parametros);
     }

    @Override
    public Reservacion obtenerPorId(final int pId) 
    {
        String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    @Override
    public List<Reservacion> obtenerTodasPorFechas(final String fechaingreso, final String fechasalida) 
    {
        String sql = new StringBuilder()
        .append(" select * from reservacion")
        .append(" where")
        .append(" ('@fechaingreso' between desde and hasta)") 
        .append(" or")
        .append(" ('@fechaingreso' >= desde and '@fechasalida' <= hasta)")
        .append(" or")
        .append(" ('@fechasalida' between desde and hasta)")
        .toString();

        sql = sql.replace("@fechaingreso",fechaingreso).replace("@fechasalida",fechasalida);

        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

}