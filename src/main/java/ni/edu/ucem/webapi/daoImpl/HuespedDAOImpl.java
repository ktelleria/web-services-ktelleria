package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.modelo.Huesped;

@Repository
public class HuespedDAOImpl implements HuespedDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public HuespedDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void agregar(final Huesped pHuedped) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO huesped")
                .append(" ")
                .append("(nombre, email, telefono)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pHuedped.getNombre();
        parametros[1] = pHuedped.getEmail();
        parametros[2] = pHuedped.getTelefono();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public Huesped obtenerPorId(final int pId) 
    {
        String sql = "select * from huesped where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }
}