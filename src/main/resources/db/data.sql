INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);

INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Matrimonial', 'Ideal para quienes viajan en pareja.',80.0);

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Vista a la piscina',1);
    
INSERT INTO cuarto (numero, descripcion,categoria)
  	VALUES(2,'Remodelado recientemente',2);

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(3,'Vista a la montaña',1);
    
INSERT INTO cuarto (numero, descripcion,categoria)
  	VALUES(4,'Cuarto de lujo',2);

INSERT INTO huesped (nombre, email, telefono)
VALUES('Kathleen Telleria','ktelleria@inss.gob.ni','22655005');

INSERT INTO reservacion (desde, hasta, cuarto,huesped)
VALUES('2017-02-25','2017-02-28',1,1);



