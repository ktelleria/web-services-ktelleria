create table usuarios (
	username VARCHAR(45) PRIMARY KEY,
	password VARCHAR(60) NOT NULL,
	enabled TINYINT NOT NULL DEFAULT 1);

create table usuarios_roles (
ID INT (11) IDENTITY PRIMARY KEY,
username VARCHAR(45) NOT NULL,
role VARCHAR(45) NOT NULL,
UNIQUE KEY usuarios_roles_uq (role,username));

alter table usuarios_roles add foreign key ( username ) references usuarios(username);