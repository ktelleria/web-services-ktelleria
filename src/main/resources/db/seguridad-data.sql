INSERT INTO usuarios(username,password,enabled)
values ('ucem','$2a$06$yaebPt1yF6dccgXxwxOJBO3rkSrqbc5REdCekImAxlFV0piTJssge',true);

INSERT INTO usuarios(username,password,enabled)
values ('ktelleria','$2a$06$yaebPt1yF6dccgXxwxOJBO3rkSrqbc5REdCekImAxlFV0piTJssge',true);

insert into usuarios_roles (username, role)
values ('ucem', 'ROLE_ADMIN');

INSERT INTO usuarios_roles (username, role)
values ('ktelleria','ROLE_USER');